package environment

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)


func LoadEnvironment(env_file string) (error) {

	// Open the .env file
	file, err := os.Open(env_file)
	if err != nil {
		fmt.Printf("unable to open .env file: %e", err)
		return err
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)

	// Loop through each line in the file
	for scanner.Scan() {
		// Split the line into a key-value pair
		line := strings.TrimSpace(scanner.Text())
		if len(line) == 0 || line[0] == '#' {
			// Skip empty lines or comments
			continue
		}
		parts := strings.SplitN(line, "=", 2)
		if len(parts) != 2 {
			fmt.Printf("invalid .env file format: %s", line)
			return err
		}
		key := parts[0]
		value := parts[1]

		// Set the environment variable
		err = os.Setenv(key, value)
		if err != nil {
			fmt.Printf("unable to set environment variable: %e", err)
			return err
		}
	}

	// Check for any errors during scanning
	if err := scanner.Err(); err != nil {
		fmt.Printf("unable to read .env file: %e", err)
		return err
	}

	return nil
}

func GetEnvDefault(key, defVal string) string {
	val, ex := os.LookupEnv(key)
	if !ex {
		return defVal
	}
	return val
}
